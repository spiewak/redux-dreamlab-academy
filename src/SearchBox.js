import React from 'react';
import { search } from './actions/storeActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export class SearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.handleOnChange = this.handleOnChange.bind(this);
    }
    
    handleOnChange(event) {
        this.props.search(event.target.value);
    }
    
    render() {
        return (
            <input type="text" onChange={this.handleOnChange}/>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({search}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBox);