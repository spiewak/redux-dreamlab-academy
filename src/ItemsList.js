import React from 'react';
import Item from './Item';
import { connect } from 'react-redux';

export class ItemsList extends React.Component {
    render() {
        return (
            <div>
            {
                this.props.items.map((item) => {
                    return <Item id={item.id} name={item.name} key={item.id} />
                })
            }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
}

export default connect(mapStateToProps)(ItemsList);