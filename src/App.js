import React, { Component } from 'react';
import ItemsList from './ItemsList';
import SearchBox from './SearchBox';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SearchBox />
        <ItemsList />
      </div>
    );
  }
}

export default App;
